from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('daftar', views.daftar, name='daftarr'),
    path('peserta/<int:pk_id>/', views.pesertaku, name='pesertaa'),
    path('delete/<int:del_id>/', views.delete, name = "delete"),
    path('deletepeserta/<int:id_del>/', views.deletepeserta, name = "deletepsrt"),
]
