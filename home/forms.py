from django import forms
from .models import Kegiatan, Peserta


class Form_Kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ('nama_kegiatan', )


class Form_Peserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ('nama_peserta', )

