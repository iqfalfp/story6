from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField('Nama Kegiatan', max_length = 30) 

    def __str__(self): 
        return self.nama_kegiatan


class Peserta(models.Model):
    nama_peserta = models.CharField('Nama Peserta', max_length = 30)
    kegiatann = models.ForeignKey(Kegiatan, on_delete=models.CASCADE , null=True, blank=True)

    def __str__(self):
        return self.nama_peserta
