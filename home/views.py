from django.shortcuts import render, redirect
from .forms import Form_Kegiatan, Form_Peserta
from .models import Kegiatan, Peserta
# Create your views here.


def index(request):
    
    if request.method == 'POST':
        isikegiatan = Form_Kegiatan(request.POST)
        if isikegiatan.is_valid():
            isikegiatan.save()
    isikegiatan = Form_Kegiatan()


    context= {"kegiatan": isikegiatan,}
    return render(request, 'index.html', context)

def pesertaku(request, pk_id):
    if request.method == 'POST':
        isipeserta = Form_Peserta(request.POST)
        if isipeserta.is_valid():
            peserta = Peserta(kegiatann = Kegiatan.objects.get(id=pk_id),
                nama_peserta = isipeserta.data['nama_peserta'])
            peserta.save()
            return redirect('home:index')
    isipeserta = Form_Peserta()
    context = {"peserta": isipeserta, }
    return render(request, 'isiorang.html', context)
    

def delete(request, del_id) :
    Kegiatan.objects.filter(id= del_id).delete()
    return redirect('home:daftarr') 


def deletepeserta(request, id_del):
    Peserta.objects.filter(id= id_del).delete()
    return redirect('home:daftarr')

def daftar(request):
    isipeserta = Form_Peserta()
    value = Kegiatan.objects.all()
    isikegiatan = Form_Kegiatan()
    psrt = Peserta.objects.all()
    
    context = {"peserta": isipeserta, "kegiatan": isikegiatan, "value":value, "psrt":psrt }
    return render(request, 'daftar.html', context)
