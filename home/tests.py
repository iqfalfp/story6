from django.test import TestCase, Client
from django.urls import resolve
from .views import pesertaku, index, daftar, delete
from .models import Kegiatan, Peserta

# Create your tests here.


class Test_Daftar_Kegiatan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_create_new_kegiatan(self):
        kegiatan = Kegiatan(nama_kegiatan="Belajar Coding")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_url_kegiatan_is_exist(self):
        response = Client().post('/', data={'nama_kegiatan': 'Coding'})
        self.assertEqual(response.status_code, 200)


class Test_daftar(TestCase):
    def test_create_url_is_exist(self):
        response = Client().get('/daftar')
        self.assertEqual(response.status_code, 200)

    def test_create_func(self):
        found = resolve('/daftar')
        self.assertEqual(found.func, daftar)

    def test_create_using_template(self):
        response = Client().get('/daftar')
        self.assertTemplateUsed(response, 'daftar.html')


class Test_Peserta(TestCase):
    def setUp(self):
        kegiatan = Peserta(nama_peserta="iqfal")
        kegiatan.save()
        self.assertEqual(Peserta.objects.all().count(), 1)

    def test_regristrasi_GET(self):
        response = self.client.get('/peserta/1/')
        self.assertTemplateUsed(response, 'isiorang.html')
        self.assertEqual(response.status_code, 200)


class TestHapusNama(TestCase):
    def setUp(self):
        kegiatanku = Kegiatan(nama_kegiatan="makan kepiting")
        kegiatanku.save()
        nama = Peserta(nama_peserta="beni")
        nama.save()

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/deletepeserta/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_url_is_exist(self):
        response = self.client.get('/deletepeserta/1/')
        self.assertEqual(response.status_code, 302)


class Test_Hapus_Kegiatan(TestCase):
    def setUp(self):
        kegiatanku = Kegiatan(nama_kegiatan="memasak kepiting")
        kegiatanku.save()
        nama = Peserta(nama_peserta="udin")
        nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/delete/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_activity_url_is_exist(self):
        response = self.client.get('/delete/1/')
        self.assertEqual(response.status_code, 302)
